echo "Instalando agent datadog..."

DD_API_KEY=858af784e234d903aec234c68a0af14e bash -c "$(curl -L https://raw.githubusercontent.com/DataDog/datadog-agent/master/cmd/agent/install_script.sh)"

echo "Instalando java agent..."

wget -O dd-java-agent.jar 'https://search.maven.org/classic/remote_content?g=com.datadoghq&a=dd-java-agent&v=LATEST'

echo "Favor adicionar o argumento na JVM: \n
        -javaagent:/path/to/the/dd-java-agent.jar"
